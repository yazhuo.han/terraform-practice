## to initialize the working directory, install providers defined in the terraform configuration

    terraform init

## to apply the configuration

    terraform apply

## apply the configuration without confirming

    terraform apply -auto-approve

## delete resources

```
1. terraform destroy

2. delete in main.tf, then terraform apply

2 is better than 1 because it's the change from the source. 
it keeps the change in the Terraform config file.
```

## preview of terraform apply

    terraform plan

## give names to a resource--tags

```tags = key-value pairs in AWS```

## terraform state

terraform.tfstate and terraform.tfstate.backup

are auto-generated on the first $ terraform apply

are JSON file where Terraform stores the state about our real world resources of our managed infrastructure

##  list all resources currently tracked in the Terraform state file

    terraform state list

## output valudes
are like function return values


## variables
= input variables

## terraform apply with variables file
terraform apply -var-file terraform-dev.tfvars

## set environmental variables for aws credentials
1. 
export AWS_SECRET_ACCESS_KEY= XXX
export AWS_ACCESS_KEY_ID=XXX

but it's not globally available

2. 
make it available in ~/.aws/credentials

aws configure

## set variable using TF environment variable
export TF_VAR_avail_zone="us-east-2a"

reference the environment variable in main.tf

